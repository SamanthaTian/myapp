/**
 * Created by dev on 8/12/16.
 */
import React, { Component } from 'react';

var user = ["sam", "mike", "siggy"];
var userLi = user.map(function(user, index){
    return (
        <li key={index}>{user}</li>
    );
})

class OutputList extends Component {
    /*getInitialState() {
        return {data: []};
    }
    componentDidMount(){
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    }
    */


    render() {
        return (
            <ul>
                {userLi}
            </ul>
        );
    }
}

export default OutputList;