import React, { Component } from 'react';
import logo from './logo.png';
import OutputList from './outputList';
import Search from './Search';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Staff Finder</h2>
        </div>

          <OutputList />
      </div>
    );
  }
}

export default App;
